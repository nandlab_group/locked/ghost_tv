#include "mainwindow.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QSettings>
#include <QtGlobal>

#include "project-info.hpp"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setApplicationName("Ghost TV");
    QApplication::setApplicationVersion(PROJECT_VERSION);

    QCommandLineParser parser;
    parser.setApplicationDescription("Fake TV, where the users have to enter a correct channel number "
                                     "on the remote to see a video with information");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.process(app);

    QApplication a(argc, argv);
    QSettings settings("locked", "ghost_tv");

    QString clientId = settings.value("clientId", "ghost_tv").toString();

    bool ok;
    int powerTimeout = settings.value("powerTimeout", "30000").toInt(&ok);

    if (!ok) {
        qFatal("Cannot parse power timeout!");
        exit(1);
    }
    if (powerTimeout < 0) {
        qFatal("Power timeout cannot be negative!");
        exit(1);
    }

    settings.beginGroup("broker");
    QString brokerHostname = settings.value("hostname", "localhost").toString();
    quint16 brokerPort = settings.value("port", 1883).toUInt();
    settings.endGroup();

    settings.beginGroup("videos");
    QString videoErrorDe = settings.value("error").toString();
    QString videoErrorEn = settings.value("error-en").toString();
    QString videoBible = settings.value("bible").toString();
    settings.endGroup();

    qDebug() << "Error video URI DE:" << videoErrorDe;
    qDebug() << "Error video URI EN:" << videoErrorEn;
    qDebug() << "Bible video URI:" << videoBible;
    qDebug() << "Broker hostname:" << brokerHostname;
    qDebug() << "Broker port:" << brokerPort;
    qDebug() << "Client ID:" << clientId;

    MainWindow w(
                videoErrorDe,
                videoErrorEn,
                videoBible,
                powerTimeout,
                brokerHostname,
                brokerPort,
                clientId);
    w.showFullScreen();
    w.startMqttConnection();
    return a.exec();
}
