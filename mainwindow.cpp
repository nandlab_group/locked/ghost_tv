#include "mainwindow.h"

#include <QCameraInfo>
#include <QVideoSurfaceFormat>
#include <QtGlobal>
#include <QApplication>
#include <QtGlobal>

static char MQTT_TOPIC_STATUS[] = "/status";

static char MQTT_TOPIC_SET_POWER[] = "/set/power";
static char MQTT_TOPIC_SET_SOLVED[] = "/set/solved";
static char MQTT_TOPIC_SET_LANG[] = "/set/lang";

static char MQTT_TOPIC_GET_SOLVED[] = "/get/solved";

static char MQTT_STATUS_WILL[] = "FAILED";
static char MQTT_STATUS_ONLINE[] = "ONLINE";
static char MQTT_STATUS_OFFLINE[] = "OFFLINE";

void MainWindow::powerOn()
{
    qInfo("Power on");
    player.setVideoOutput(&video);
    playlist.setCurrentIndex(0);
    player.play();
    power = true;
}

void MainWindow::powerOff()
{
    qInfo("Power off");
    player.setVideoOutput((QVideoWidget *) nullptr);
    player.stop();
    powerTimer.stop();
    playlist.setCurrentIndex(0);
    power = false;
}

void MainWindow::setUpPlaylist(int lang)
{
    playlist.clear();
    playlist.addMedia(QUrl::fromLocalFile(lang == LANG_DE ? errorVideoUriDe : errorVideoUriEn));
    playlist.addMedia(QUrl::fromLocalFile(bibleVideoUri));
    playlist.setCurrentIndex(0);
    playlist.setPlaybackMode(QMediaPlaylist::CurrentItemInLoop);
    powerOff();
}

void MainWindow::lircAppString(QSharedPointer<QString> appString)
{
    if (*appString == "power"){
        qInfo("Power pressed");
        powerTimer.stop();
        // biblePlayer.stop();
        if (!power) {
            powerOn();
        }
        else {
            powerOff();
        }
    }
    if (power && *appString == "code_entered") {
        qInfo("Code entered correctly");
        solve();
    }
}

void MainWindow::timerCallback()
{
    qDebug("Timer callback called");
    powerOff();
}

MainWindow::MainWindow(
        const QString &errorVideoDe,
        const QString &errorVideoEn,
        const QString &bibleVideo,
        unsigned powerTimeout,
        const QString &brokerHostname,
        quint16 brokerPort,
        const QString &clientId,
        QWidget *parent)
    : QMainWindow(parent)
    , lircClientThread()
    , lircClient("ghost_tv")
    , errorVideoUriDe(errorVideoDe)
    , errorVideoUriEn(errorVideoEn)
    , bibleVideoUri(bibleVideo)
    , mqttClient()
    , first(true)
    , video()
    , playlist()
    , player()
    , power(false)
    , powerTimer()
{
    qDebug("MainWindow: Constructing...");

    setCentralWidget(&video);

    if (!lircClient.success()) {
        qFatal("Cannot construct lircClient!");
        exit(1);
    }

    connect(&lircClient, &LircClient::appString, this, &MainWindow::lircAppString);
    connect(&lircClientThread, &QThread::started, &lircClient, &LircClient::run);

    connect(&powerTimer, &QTimer::timeout, this, &MainWindow::timerCallback);

    lircClient.moveToThread(&lircClientThread);
    lircClientThread.start();

    mqttClient.setHostname(brokerHostname);
    mqttClient.setPort(brokerPort);
    mqttClient.setClientId(clientId);
    mqttClient.setWillTopic(clientId + MQTT_TOPIC_STATUS);
    mqttClient.setWillMessage(MQTT_STATUS_WILL);
    mqttClient.setWillRetain(true);

    reconnectTimer.setSingleShot(true);
    reconnectTimer.setInterval(1000);

    connect(QApplication::instance(), &QApplication::aboutToQuit, this, &MainWindow::aboutToQuit);

    connect(&reconnectTimer, &QTimer::timeout, this, &MainWindow::reconnect);

    connect(&mqttClient, &QMqttClient::stateChanged, this, &MainWindow::stateChanged);
    connect(&mqttClient, &QMqttClient::connected, this, &MainWindow::connected);
    connect(&mqttClient, &QMqttClient::disconnected, this, &MainWindow::disconnected);
    connect(&mqttClient, &QMqttClient::errorChanged, this, &MainWindow::errorChanged);
    connect(&mqttClient, &QMqttClient::messageSent, this, &MainWindow::messageSent);

    setUpPlaylist(LANG_DE);

    video.setAutoFillBackground(true);
    video.setStyleSheet("background-color: black;");

    setAutoFillBackground(true);
    setStyleSheet("background-color: black;");

    player.setPlaylist(&playlist);

    powerTimer.setSingleShot(true);
    powerTimer.setInterval(powerTimeout);

    qDebug("MainWindow: Done.");
}

void MainWindow::solve()
{
    player.setVideoOutput(&video);
    playlist.setCurrentIndex(1);
    player.play();
    powerTimer.start();
    mqttClient.publish(mqttClient.clientId() + MQTT_TOPIC_GET_SOLVED, "1");
}

void MainWindow::startMqttConnection()
{
    mqttClient.connectToHost();
}

void MainWindow::stateChanged(QMqttClient::ClientState state) {
    qDebug() << "State:" << state;
}

void MainWindow::connected() {
    qDebug("Connected");

    QString clientId = mqttClient.clientId();
    QString topicStatus = clientId + MQTT_TOPIC_STATUS;

    qDebug("Subscribing to setter topics");
    QMqttSubscription *sub = mqttClient.subscribe(clientId + "/set/#");
    qDebug() << "Sending status:" << MQTT_STATUS_ONLINE;
    mqttClient.publish(topicStatus, MQTT_STATUS_ONLINE, 0, true);
    connect(sub, &QMqttSubscription::messageReceived, this, &MainWindow::messageReceived);
}

void MainWindow::disconnected() {
    qDebug("Disconnected");
    reconnectTimer.start();
}

void MainWindow::errorChanged(QMqttClient::ClientError error) {
    qDebug() << "Error:" << error;
}

void MainWindow::messageSent(qint32 id) {
    qDebug() << "Message with id" << id << "was sent";
}

void MainWindow::reconnect() {
    qDebug("Reconnecting ...");
    mqttClient.connectToHost();
}

void MainWindow::aboutToQuit() {
    qDebug() << "Sending status:" << MQTT_STATUS_OFFLINE;
    mqttClient.publish(mqttClient.clientId() + MQTT_TOPIC_STATUS, MQTT_STATUS_OFFLINE, 0, true);
}

void MainWindow::messageReceived(QMqttMessage message) {
    QString clientId = mqttClient.clientId();
    QString topic = message.topic().name();
    QString payload = message.payload();
    qDebug() << "Message received on topic" << topic << ":" << payload;
    if (topic == mqttClient.clientId() + MQTT_TOPIC_SET_POWER) {
        if (payload.startsWith("0")) {
            powerOff();
        }
        else {
            powerOn();
        }
    }
    else if (topic == mqttClient.clientId() + MQTT_TOPIC_SET_LANG) {
        int lang = payload.toLower().startsWith("de") ? LANG_DE : LANG_EN;
        qDebug() << "Language:" << (lang == LANG_DE ? "de" : "en");
        setUpPlaylist(lang);
    }
    else if (topic == mqttClient.clientId() + MQTT_TOPIC_SET_SOLVED) {
        qInfo("Solved by the admin");
        powerOn();
        solve();
    }
}
