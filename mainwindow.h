#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVideoWidget>
#include <QMediaContent>
#include <QMediaPlayer>
#include <QStackedLayout>
#include <QHBoxLayout>
#include <QCamera>
#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QAbstractVideoSurface>
#include <QVideoFrame>
#include <QThread>
#include "lircclient.h"
#include <QTimer>
#include <QMediaPlaylist>
#include <QMqttClient>

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QThread lircClientThread;
    LircClient lircClient;

    QString errorVideoUriDe;
    QString errorVideoUriEn;
    QString bibleVideoUri;

    QTimer reconnectTimer;
    QMqttClient mqttClient;

    bool first;

    QVideoWidget video;

    QMediaPlaylist playlist;
    QMediaPlayer player;
    bool power;

    QTimer powerTimer;

    void powerOn();

    void powerOff();

    enum {
        LANG_DE,
        LANG_EN
    };

    void setUpPlaylist(int lang);

private slots:
    void lircAppString(QSharedPointer<QString> appString);

    void timerCallback();

    void stateChanged(QMqttClient::ClientState state);

    void connected();

    void disconnected();

    void errorChanged(QMqttClient::ClientError error);

    void messageSent(qint32 id);

    void reconnect();

    void aboutToQuit();

    void messageReceived(QMqttMessage message);

    void solve();

public:
    explicit
    MainWindow(const QString &errorVideoDe,
               const QString &errorVideoEn,
               const QString &bibleVideo,
               unsigned powerTimeout = 30000,
               const QString &brokerHostname = "localhost",
               quint16 brokerPort = 1883,
               const QString &clientId = "ghost_tv",
               QWidget *parent = nullptr);

    void startMqttConnection();
};
#endif // MAINWINDOW_H
