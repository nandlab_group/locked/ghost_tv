#ifndef LIRCCLIENT_H
#define LIRCCLIENT_H

#include <QObject>
#include <lirc_client.h>
#include <cstring>
#include <QString>
#include <QSharedPointer>
#include <QtGlobal>

class LircClient : public QObject
{
    Q_OBJECT

    int _fd;
    struct lirc_config* _config;

    static const int _sharedStringType;

public:
    int success()
    {
        return _fd != -1 && _config != NULL;
    }

    explicit LircClient(QObject *parent = nullptr)
        : _fd(-1), _config(nullptr)
    {}

    explicit LircClient(const QString &clientName, QObject *parent = nullptr)
        : _config(nullptr)
    {
        _fd = lirc_init(clientName.toUtf8().data(), 1);
        if (_fd != -1)
        {
            if (lirc_readconfig(NULL, &_config, NULL) == -1)
            {
                _config = nullptr;
            }
        }
    }

    explicit LircClient(const char *clientName, QObject *parent = nullptr)
        : _config(nullptr)
    {
        _fd = lirc_init(clientName, 1);
        if (_fd != -1)
        {
            if (lirc_readconfig(NULL, &_config, NULL) == -1)
            {
                _config = nullptr;
            }
        }
    }

    int run()
    {
        using std::strerror;
        char *code = nullptr;
        char *str = nullptr;
        int ret = 0;
        while ((ret = lirc_nextcode(&code)) == 0)
        {
            if (!code)
                continue;
            qDebug("LIRC code: %s", code);
            int result = lirc_code2char(_config, code, &str);
            free(code);
            if (result == -1) {
                qWarning("lirc_code2char failed: %s!", strerror(errno));
            }
            QSharedPointer<QString> appStringPointer = QSharedPointer<QString>::create(str);
            emit appString(appStringPointer);
            code = nullptr;
            str = nullptr;
        }
        qFatal("lirc_nextcode failed: %s!", strerror(errno));
        return ret;
    }

    ~LircClient()
    {
        lirc_freeconfig(_config);
        lirc_deinit();
    }

signals:
    void appString(QSharedPointer<QString> code);
};

Q_DECLARE_METATYPE(QSharedPointer<QString>);

#endif // LIRCCLIENT_H
